import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
})
export class HomeComponent implements OnInit {
  gridColumns = 3;
  wordsData: any;
  charactersTotal: number;
  wordsTotal: number;
  facebookStatusTotal: number = 250;
  facebookStatusPercent: number = 0;
  twitterStatusTotal: number = 280;
  twitterStatusPercent: number = 0;

  constructor() {}

  ngOnInit(): void {
    this.charactersTotal = 0;
    this.wordsTotal = 0;
  }

  changeWords(event: Event) {
    this.wordsData = event;
    this.charactersTotal = this.wordsData.length;
    let wordsArray = this.wordsData.split(/[\s\n]+/);
    this.wordsTotal = wordsArray.length;
    if (this.charactersTotal == 0) {
      this.wordsTotal = 0;
    }

    this.facebookStatusPercent = parseFloat(
      ((this.charactersTotal / this.facebookStatusTotal) * 100).toFixed(1)
    );

    this.twitterStatusPercent = parseFloat(
      ((this.charactersTotal / this.twitterStatusTotal) * 100).toFixed(1)
    );
  }

  deleteWords() {
    this.wordsData = '';
    this.charactersTotal = 0;
    this.wordsTotal = 0;
  }
}
